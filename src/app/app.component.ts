import {Component} from '@angular/core';
import 'hammerjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  hero = 'kitty';
  kittyFlip = false;
  dragonFlip = false;
  current = null;
  SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
  show = true;

  constructor() {
    this.setKitty(null);
  }

  ngOnInit() {
  }

  setKitty(event) {
    this.hero = 'kitty';
    this.current = this.hero;
    let audio = new Audio('../assets/meow.mp3');
    audio.play();
  }

  setFeed(event) {
    this.hero = 'feed';
    this.current = this.hero;
  }

  setDragon(event) {
    this.hero = 'dragon';
    this.current = this.hero;
    let audio = new Audio('../assets/roar.mp3');
    audio.play();
  }

  toogleKitty(event) {
    this.kittyFlip = !this.kittyFlip;
  }

  toogleDragon(event) {
    this.dragonFlip = !this.dragonFlip;
  }

  swipe(action) {
    if (action === this.SWIPE_ACTION.LEFT) {
      if(this.current === 'kitty'){
        this.setFeed(null);
      } else if(this.current === 'dragon'){
        this.setKitty(null)
      }
      return;
    }
    if (action === this.SWIPE_ACTION.RIGHT) {
      if(this.current === 'kitty'){
        this.setDragon(null)
      } else if(this.current === 'feed'){
        this.setKitty(null)
      }
      return;
    }
  }

  showNav(event){
    if(!this.show){
      this.show = true;
      return;
    }
    this.show = false;
  }

}

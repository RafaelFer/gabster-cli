/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GabsterComponent } from './gabster.component';

describe('GabsterComponent', () => {
  let component: GabsterComponent;
  let fixture: ComponentFixture<GabsterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GabsterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GabsterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

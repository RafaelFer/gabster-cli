import { GabsterCliPage } from './app.po';

describe('gabster-cli App', function() {
  let page: GabsterCliPage;

  beforeEach(() => {
    page = new GabsterCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
